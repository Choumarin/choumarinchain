const varPeers = require('./peers');

module.exports = {

  p2p_port: process.env.P2P_PORT || 6000,
  http_port: process.env.HTTP_PORT || 3000,
  difficulty: "1234",
  initialPeers: process.env.PEERS ? process.env.PEERS.split(',') : [],
  peers: varPeers.peers,
  currentMiningThread: undefined,
  busy : undefined,
  nodeIdentifier: null,

}
