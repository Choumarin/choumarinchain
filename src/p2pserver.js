
const WebSocket = require("ws");
const ip = require("ip");

//
const variables = require('./variables.js');
const datachain = require('./datachain');
const MessageData = require('./messageData');
const MessageType = require('./messageType')
const connexionHandler = require('./connexionHandler')
const blockchain = require('./blockchain')
const logger = require("./log.js");

//
const p2p_port = variables.p2p_port

const initP2PServer = () => {
    const server = new WebSocket.Server({ port: p2p_port });
    server.on('connection', ws => initConnection(ws));
    logger.info('listening websocket p2p port on: ' + p2p_port);
};


var initConnection = (ws) => {
    datachain.sockets.push(ws);
    initMessageHandler(ws);
    initErrorHandler(ws);
    connexionHandler.write(ws, MessageData.queryChainLengthMsg());
    connexionHandler.write(ws, MessageData.peerInfo())
    connexionHandler.write(ws, MessageData.getMemoryPool());

};
var connectToPeers = (newPeers) => {
    newPeers.forEach((peer) => {
        //Prevent connection to localhost
        if (!peer.indexOf('localhost') > -1) {
            const ws = new WebSocket(peer);
            ws.on('open', () => initConnection(ws));
            ws.on('error', () => {
                logger.error('Connection failed')
            });
        } else {
            logger.warn("Can't connect to localhost");
        }
    });
};
var initMessageHandler = (ws) => {
    ws.on('message', (data) => {
        const message = JSON.parse(data);
        logger.info("Received message: \n" + JSON.stringify(message,null,4));
        switch (message.type) {
            case MessageType.QUERY_LATEST:
                connexionHandler.write(ws, MessageData.responseLatestMsg());
                break;
            case MessageType.QUERY_ALL:
                connexionHandler.write(ws, MessageData.responseChainMsg());
                break;
            case MessageType.RESPONSE_BLOCKCHAIN:
                handleBlockchainResponse(message);
                break;
            case MessageType.MINE_BLOCK:
                if (variables.currentMiningThread === undefined && variables.busy === undefined) {
                    blockchain.startMining(message.data);
                }
                break;
            case MessageType.ADD_TO_MEMORYPOOL:
                blockchain.addToMemoryPool(message.data)
                break;
            case MessageType.QUERY_MEMORYPOOL:
                connexionHandler.write(ws, MessageData.responseMemoryPool());
                break;
            case MessageType.RESPONSE_MEMORYPOOL:
                if (datachain.memoryPool.length <= message.data.length) {
                    datachain.memoryPool = message.data;
                } else {
                    logger.warn("Memorypool shorter")
                }
                break;
            case MessageType.BLOCK_MINED:
                if (variables.currentMiningThread !== undefined) {
                    variables.currentMiningThread.kill();
                    variables.currentMiningThread = undefined
                }
                blockchain.removeFromMemoryPool(message)
                logger.debug("new block mined by " + message.id);
                break;
            case MessageType.PEER_INFO:
                if (datachain.peers.indexOf(message.data) === -1) {
                    datachain.peers.push(message.data);
                    connexionHandler.broadcast(MessageData.newPeer(message.data))
                }
                break;
            case MessageType.NEW_PEER:
                if (datachain.peers.indexOf(message.data) === -1 && message.data !== ('ws://' + ip.address() + ':' + variables.p2p_port)) {
                    datachain.peers.push(message.data);
                    connectToPeers([message.data])
                }
                break;
        }
    });
};
var initErrorHandler = (ws) => {
    const closeConnection = (ws) => {
        logger.error('Connection failed to peer: ' + ws.url)
        datachain.peers.splice(datachain.peers.indexOf(ws.url), 1);
        datachain.sockets.splice(datachain.sockets.indexOf(ws), 1);
    };
    ws.on('close', () => closeConnection(ws));
    ws.on('error', () => closeConnection(ws));
};
var handleBlockchainResponse = (message) => {
    const receivedBlocks = JSON.parse(message.data).sort((b1, b2) => (b1.index - b2.index));
    logger.debug("Received blocks " + JSON.stringify(receivedBlocks, null, 4));
    const latestBlockReceived = receivedBlocks[receivedBlocks.length - 1];
    const latestBlockHeld = MessageData.getLatestBlock();
    if (latestBlockReceived.index > latestBlockHeld.index) {
        logger.debug('blockchain possibly behind. We got: ' + latestBlockHeld.index + ' Peer got: ' + latestBlockReceived.index);
        if (latestBlockHeld.hash === latestBlockReceived.previousHash) {
            logger.debug("We can append the received block to our chain");
            blockchain.addBlock(latestBlockReceived);
            connexionHandler.broadcast(MessageData.responseLatestMsg());
        } else if (receivedBlocks.length === 1) {
            logger.debug("We have to query the chain from our peer, the latest block's hash don't match");
            variables.busy = "We have to query the chain from our peer, the latest block's hash don't match";
            connexionHandler.broadcast(MessageData.queryAllMsg());
        } else {
            logger.debug("Received blockchain is longer than current blockchain");
            if(variables.busy !== undefined) {
                logger.debug("I was busy: " + variables.busy + "END");
                variables.busy = undefined
            }
            blockchain.replaceChain(receivedBlocks);
        }
    } else {
        logger.debug('Received blockchain is not longer than current blockchain. Do nothing');
    }
};

module.exports = {
    initP2PServer: initP2PServer,
    initConnection, initConnection,
    connectToPeers, connectToPeers,
}
