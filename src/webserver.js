const express = require("express");
const favicon = require("serve-favicon");
const bodyParser = require('body-parser');
const { body, validationResult } = require('express-validator/check');
//
const variables = require('./variables');
const MessageData = require('./messageData');
const datachain = require('./datachain');
const blockchain = require('./blockchain')
const p2pServer = require('./p2pserver')
const connexionHandler = require('./connexionHandler')
const path = require('path');
const logger = require('./log.js');
//

module.exports = {
  initHttpServer: function () {
    const app = express();
    app.use(bodyParser.json());
    app.use(favicon(path.join(__dirname, 'web', 'img', 'favicon.ico')));

    app.get('/', (req, res) => {
        return res.redirect("/web/")
    });

    app.get('/web/', (req, res) => {
        res.sendFile(path.join(__dirname + '/web/index.html'));
    })

    app.get('/web/stats', (req, res) => {
        res.sendFile(path.join(__dirname + '/web/stats.html'))
    })

    app.get('/web/logo', (req, res) => {
        res.sendFile(path.join(__dirname + '/web/img/logo.png'))
    })

    app.get('/web/blocks', (req, res) => {
        res.sendFile(path.join(__dirname + '/web/blocks.html'));
    })

    app.get('/web/peers', (req, res) => {
        res.sendFile(path.join(__dirname + '/web/peers.html'));
    })

    app.get('/web/pool', (req, res) => {
        res.sendFile(path.join(__dirname + '/web/pool.html'));
    })

    app.get('/blocks', (req, res) => res.send(JSON.stringify(datachain.getBlockchain()) + '\n'));
    app.get('/id', (req, res) => {
      res.send(variables.nodeIdentifier + '\n');
    });
    app.post('/mineBlock', [
      body('id', 'No id provided').exists(),
      body('date', 'No date provided').exists(),
      body('action', 'No action provided').exists(),
      body('author', 'No author provided').exists()
    ], (req, res) => {

      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        return res.status(422).json({ errors: errors.array() });
      }
      var block = req.body
      if (variables.currentMiningThread === undefined && variables.busy === undefined && datachain.memoryPool.length === 0) {
        logger.debug('Block sent to thread from web-server' + JSON.stringify(block, null, 4))
        blockchain.startMining(block);
        connexionHandler.broadcast(MessageData.mineBlock(block));
        res.status(200).send("Starting to mine \n");

      }
      else if (datachain.memoryPool) {
        if(variables.busy!==undefined){
          logger.debug('Node busy to synchronize')
          logger.debug("I'm busy: " + variables.busy);
        }
        blockchain.addToMemoryPool(block)
        //TODO Move to P2Pserver
        connexionHandler.broadcast(MessageData.addToMemoryPool(block))
        res.status(200).send("Saved to memoryPool \n");
        logger.debug('Saved to memorypool: ' + JSON.stringify(block, null, 4))
      } else {
        res.status(500).send('Error Critique, not minning and no memorypool');
        logger.error('Error Critique, not minning and no memorypool')
      }
    });
    app.get('/blocks/:id', (req, res) => {
      blocks  = blockchain.findBlocksById(req.params.id)
      if (!blocks.length){
        res.status(404).send("No blocks with that ID");
      }else{
        res.status(200).send(blocks);
      }
    });
    app.get('/peers', (req, res) => {
      res.send(JSON.stringify(datachain.peers) + '\n');
    });
    app.get('/sockets', (req, res) => {
      res.send(sockets.map(s => s._socket.remoteAddress + ':' + s._socket.remotePort))
    });
    app.post('/addPeer', (req, res) => {
      p2pServer.connectToPeers([req.body.peer]);
      res.send('Peer added \n');
    });
    app.get('/memorypool', (req, res) => {
      res.send(JSON.stringify(datachain.memoryPool) + '\n');
    });
    app.get('*', function(req, res){
      res.status(400).sendFile(path.join(__dirname + '/web/pagenotfound.html'));
    });
    app.listen(variables.http_port, () => logger.info('Listening http on port: ' + variables.http_port + '\n'));
  }

};
