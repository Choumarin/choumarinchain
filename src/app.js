'use strict';
// //Internal Dependencies
const variables = require('./variables.js')
// const Block = require("./Block");
const webServer = require('./webserver');
const p2pServer = require('./p2pserver');
const datachain = require('./datachain');
const logger = require('./log.js');

// //variables

variables.nodeIdentifier = require("randomstring").generate(6)

datachain.initblockchain();
p2pServer.initP2PServer();
webServer.initHttpServer();
p2pServer.connectToPeers(variables.initialPeers)
p2pServer.connectToPeers(variables.peers);

logger.info("Starting node " + variables.nodeIdentifier);
