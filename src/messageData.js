const ip = require("ip");
const variables = require('./variables.js')
const MessageType = require('./messageType');
const datachain = require('./datachain');

var getLatestBlock = () => { 
    blockchain = (datachain.getBlockchain());
    return blockchain[blockchain.length - 1];
 };
var getMemoryPool = () => ({
    'type': MessageType.QUERY_MEMORYPOOL
});
var queryChainLengthMsg = () => ({
    'type': MessageType.QUERY_LATEST
});
var queryAllMsg = () => ({
    'type': MessageType.QUERY_ALL
});
var responseChainMsg = () => ({
    'type': MessageType.RESPONSE_BLOCKCHAIN,
    'data': JSON.stringify(datachain.getBlockchain(),null,4)
});
var responseLatestMsg = () => ({
    'type': MessageType.RESPONSE_BLOCKCHAIN,
    'data': JSON.stringify([getLatestBlock()])
});
var responseMemoryPool = () => ({
    'type': MessageType.RESPONSE_MEMORYPOOL,
    'data': datachain.memoryPool
});
var mineBlock = (data) => ({
    'type': MessageType.MINE_BLOCK,
    'data': data
});
var addToMemoryPool = (data) => ({
    'type': MessageType.ADD_TO_MEMORYPOOL,
    'data': data
});
var blockMined = (block) => ({
    'type': MessageType.BLOCK_MINED,
    'data' : block.data,
    'id': variables.nodeIdentifier
});
var peerInfo = () => ({
    'type': MessageType.PEER_INFO,
    'data': 'ws://' + ip.address() + ':' + variables.p2p_port
});
var newPeer = (peer) => ({
    'type': MessageType.NEW_PEER,
    'data': peer
});
module.exports = {
    getMemoryPool: getMemoryPool,
    getLatestBlock: getLatestBlock,
    queryChainLengthMsg: queryChainLengthMsg,
    queryAllMsg: queryAllMsg,
    responseChainMsg: responseChainMsg,
    responseLatestMsg: responseLatestMsg,
    responseMemoryPool: responseMemoryPool,
    mineBlock: mineBlock,
    addToMemoryPool: addToMemoryPool,
    blockMined: blockMined,
    peerInfo: peerInfo,
    newPeer: newPeer,
}
