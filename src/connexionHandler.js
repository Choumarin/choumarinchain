const WebSocket = require("ws");
//
const datachain = require('./datachain');

var write = (ws, message) => ws.send(JSON.stringify(message));
var broadcast = (message) => datachain.sockets.forEach(socket => write(socket, message));

module.exports = {
    broadcast: broadcast,
    write: write,
}
