const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, prettyPrint, colorize } = format;

const logger = createLogger({
  level: process.env.NODE_ENV == "debug" ? "debug" : "info",
  format: combine(
    format.timestamp({format:'YYYY-MM-DD HH:mm:ss'}),
    format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)
  ),
  transports: [
    new transports.Console({
      format: combine(
        colorize(),
        format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)        
      )
    }),
    new transports.File({ filename: 'logs/error.log', level: 'error' }),
    new transports.File({ filename: 'logs/combined.log' })
  ]
});

module.exports = logger;