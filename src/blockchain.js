//
const CryptoJS = require("crypto-js");
const WebSocket = require("ws");
//
const spawn = require("threads").spawn;
const miner = require("./miner");
const variables = require('./variables.js')
const datachain = require('./datachain');
const MessageData = require('./messageData');
const Block = require("./block");
const connexionHandler = require('./connexionHandler')
const logger = require("./log.js");

//


var startMining = (data) => {
    const minerThread = spawn(miner);
    variables.currentMiningThread = minerThread;
    logger.debug("Minning new block" + JSON.stringify(data));
    minerThread
        .send({
            nextBlock: data,
            previousBlock: MessageData.getLatestBlock(),
            difficulty: variables.difficulty,
            nodeIdentifier: variables.nodeIdentifier
        })
        .on("message", function (response) {

            try {
                if (variables.currentMiningThread !== undefined) {
                    variables.currentMiningThread.kill()
                    variables.currentMiningThread = undefined
                }
                else {
                    logger.error('Process already killed by p2p')
                    throw ('Process already killed by p2p');
                }
            }
            catch (err) {
                logger.error("Error on finilization of minning " + err);
            }
            if (datachain.getBlockchain[response.nextIndex] !== undefined) {
                logger.warning('Block already added : ' + JSON.stringify(response))
                return;
            } // do nothing if block already exists


            const newBlock = new Block(
                response.nextIndex,
                response.previousBlockHash,
                response.nextTimestamp,
                variables.nodeIdentifier, //Miner name
                response.nextHash,
                response.nonce,
                //Blockument Data
                response.blockData

            );
            // Add block to local
            addBlock(newBlock);
            //Broadcast that a block was mined with the author
            removeFromMemoryPool(newBlock)
            connexionHandler.broadcast(MessageData.blockMined(newBlock));
            //Checks if the latest that he mined is the latest block of everyone
            connexionHandler.broadcast(MessageData.responseLatestMsg());
            //Thread management
            //Get next block to mine from memoryPool
            if (datachain.memoryPool.length) {
                startMiningFromMemoryPool()
            }

        });
};


var startMiningFromMemoryPool = () => {
    //Shift return first elementof array (FIFO)
    try {
        if (datachain.memoryPool.length) {
            var blocktomine = datachain.memoryPool.shift()
            logger.debug('Block sent to thread from memorypool: '+ JSON.stringify(blocktomine))
            startMining(blocktomine);
            connexionHandler.broadcast(MessageData.mineBlock(blocktomine));
        } else {
            logger.warn("Memorypoool empty")
            throw ("MemoryPool empty");
        }
    }
    catch (exception) {
        logger.warn(exception);
    }

}
var removeFromMemoryPool = (block) => {
    datachain.memoryPool = datachain.memoryPool.filter(function (blockToRemove) {
        logger.debug('Block removed from memorypool: ' + JSON.stringify(block, null, 4))
        return blockToRemove.date !== block.data.date;
    });
};
var findBlocksById = (id) => {
    var document = []
    logger.debug('Was requested for finding block with id ' + id);
    datachain.getBlockchain().forEach(block => {
        if (block.data.id == id) {
            document.push(block)
        }
    });
    
    return document
}
const calculateHashForBlock = (block) => {
    return calculateHash(block.index, block.previousHash, block.timestamp, block.data, block.nonce, block.miner);
};

var calculateHash = (index, previousHash, timestamp, data, nonce, miner) => {
    return CryptoJS.SHA256(index + previousHash + timestamp + data + nonce.toString() + miner).toString();
};

var addBlock = (newBlock) => {
    if (isValidNewBlock(newBlock, MessageData.getLatestBlock())) {
        datachain.addToBlockchain(newBlock);
        logger.debug('Block added: ' + JSON.stringify(newBlock, null, 4));

    }
};
var addToMemoryPool = (block) => {
    logger.debug('Block added to memorypool : ' + JSON.stringify(block))
    datachain.memoryPool.push(block);
}
var isValidNewBlock = (newBlock, previousBlock) => {
    logger.debug('Checking new block: ' + JSON.stringify(newBlock), null, 4 + "\n Previous block: " + JSON.stringify(previousBlock, null, 4))
    if (previousBlock.index + 1 !== newBlock.index) {
        logger.error("Invalid index");
        return false;
    } else if (previousBlock.hash !== newBlock.previousHash) {
        logger.error("Invalid previous hash");
        return false;
    } else if (calculateHashForBlock(newBlock) !== newBlock.hash) {
        logger.debug(JSON.stringify(newBlock, null, 4));
        logger.error('Invalid hash: ' + calculateHashForBlock(newBlock) + ' ' + newBlock.hash);
        return false;
    }
    return true;
};

var replaceChain = (newBlocks) => {
    if (isValidChain(newBlocks) && newBlocks.length > datachain.getBlockchain().length) {
        logger.debug('Received blockchain is valid. Replacing current blockchain with received blockchain')
        datachain.setBlockchain(newBlocks);
        connexionHandler.broadcast(MessageData.responseLatestMsg());
    } else {
        logger.debug('Received blockchain invalid');
    }
};

var isValidChain = (blockchainToValidate) => {
    if (JSON.stringify(blockchainToValidate[0]) !== JSON.stringify(datachain.getGenesisBlock())) {
        return false;
    }
    const tempBlocks = [blockchainToValidate[0]];
    for (let i = 1; i < blockchainToValidate.length; i++) {
        if (isValidNewBlock(blockchainToValidate[i], tempBlocks[i - 1])) {
            tempBlocks.push(blockchainToValidate[i]);
        } else {
            return false;
        }
    }
    return true;
};

module.exports = {
    addToMemoryPool: addToMemoryPool,
    startMining: startMining,
    addBlock: addBlock,
    replaceChain: replaceChain,
    removeFromMemoryPool: removeFromMemoryPool,
    findBlocksById: findBlocksById,
}
