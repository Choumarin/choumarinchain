var LocalStorage = require('node-localstorage').LocalStorage;
localStorage = new LocalStorage('./data');
const Block = require("./block");


function getGenesisBlock() {
  return new Block(0, "0", 1465154705, "genesis", "816534932c2b7154836da6afc367695e6337db8a921823784c14378abed4f000", "", "origin");
}
var initblockchain = () => {
  blockchain  = [getGenesisBlock()];
  setBlockchain(blockchain);
}

var getBlockchain = () =>{
    return JSON.parse(localStorage.getItem('blockchain'));
}
var setBlockchain = (blockchain) => {
    localStorage.setItem('blockchain', JSON.stringify(blockchain));
}
var addToBlockchain = (block) => {
  blockchain = (getBlockchain());
  blockchain.push(block);
  setBlockchain(blockchain);
}

module.exports = {
  sockets: [],
  peers: [],
  memoryPool: [],
  initblockchain:initblockchain,
  setBlockchain:setBlockchain,
  getBlockchain:getBlockchain,
  addToBlockchain: addToBlockchain,
  getGenesisBlock: getGenesisBlock,
}
