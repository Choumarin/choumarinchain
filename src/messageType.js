const MessageType = {
    QUERY_LATEST: 0,
    QUERY_ALL: 1,
    RESPONSE_BLOCKCHAIN: 2,
    MINE_BLOCK: 3,
    BLOCK_MINED: 4,
    PEER_INFO: 5,
    NEW_PEER: 6,
    ADD_TO_MEMORYPOOL: 7,
    QUERY_MEMORYPOOL: 8,
    RESPONSE_MEMORYPOOL: 9

};

module.exports = MessageType;
