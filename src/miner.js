module.exports = function (input, done) {

    const randomstring = require("randomstring");
    const CryptoJS = require("crypto-js");

    const calculateHash = (index, previousHash, timestamp, data, nonce, author) => {
        return CryptoJS.SHA256(index + previousHash + timestamp + data + nonce.toString() + author).toString();
    };

    const blockData = input.nextBlock;
    const previousBlock = input.previousBlock;
    const nextIndex = previousBlock.index + 1;
    const nextTimestamp = new Date().getTime() / 1000;
    var author = input.nodeIdentifier;

    var nonce = randomstring.generate();
    let nextHash = calculateHash(nextIndex, previousBlock.hash, nextTimestamp, blockData, nonce, author);
    while (!nextHash.endsWith(input.difficulty)) {
        nonce = randomstring.generate();
        nextHash = calculateHash(nextIndex, previousBlock.hash, nextTimestamp, blockData, nonce, author);
    }

    done({
        nextIndex: nextIndex,
        previousBlockHash: previousBlock.hash,
        nextTimestamp: nextTimestamp,
        blockData: blockData,
        nextHash: nextHash,
        nonce: nonce
    })
};
