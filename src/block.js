class Block {
    constructor(index, previousHash, timestamp, miner, hash, nonce,  data) {
        this.index = index;
        this.previousHash = previousHash.toString();
        this.timestamp = timestamp;
        this.miner = miner;
        this.hash = hash.toString();
        this.nonce = nonce;
        //Blockument data
        this.data = data;
       
    }
}

module.exports = Block;
