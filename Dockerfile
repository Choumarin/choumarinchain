FROM node:carbon

RUN mkdir /naivechain
ADD src/ /naivechain/

RUN cd /naivechain && npm install

EXPOSE 3001
EXPOSE 6001
ENV NODE_ENV=debug
ENTRYPOINT cd /naivechain && npm install && npm start
